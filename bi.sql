SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `t1bi` ;
USE `t1bi` ;

-- -----------------------------------------------------
-- Table `t1bi`.`daily_due_report`
-- This table stores the ids of users who havent returned the item before a given date.
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t1bi`.`daily_due_report` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `itemId` INT NULL,
  `userId` VARCHAR(100) NULL,
  `due_date` DATE NULL,
  `report_date` DATE NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `t1bi`.`weekly_products_assigned`
-- This table stores the count of items of a Product that was assigned a week before a given date.
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `t1bi`.`weekly_products_assigned` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `productId` INT NULL,
  `count` INT NULL,
  `report_date` DATE NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
